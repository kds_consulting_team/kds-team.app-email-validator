import logging
import csv
from enum import Enum
import validators.sendgrid_validator
from keboola.component.base import ComponentBase, UserException
import time

# configuration variables
KEY_API_KEY = "#api_key"
KEY_API_VALIDATION_SERVICE = "api_service"
KEY_EMAIL_COLUMN_NAME = 'email'
KEY_EMAIL_SOURCE_COLUMN_NAME = 'source'

KEY_LOADING_OPTIONS = "loading_options"
KEY_LOADING_OPTIONS_INCREMENTAL = "incremental"
KEY_LOADING_OPTIONS_PKEY = "pkey"

# Set max run time to 90% of max runtime of component so there is enough time to write results
MAX_RUNTIME_SECONDS = 3000
LOGGING_MAILS_FREQUENCY = 100

REQUIRED_PARAMETERS = [KEY_API_VALIDATION_SERVICE, KEY_API_KEY, KEY_EMAIL_COLUMN_NAME]
REQUIRED_IMAGE_PARS = []


class Component(ComponentBase):

    def __init__(self):
        super().__init__(required_parameters=REQUIRED_PARAMETERS,
                         required_image_parameters=REQUIRED_IMAGE_PARS)

    def run(self):
        params = self.configuration.parameters

        validation_service = params.get(KEY_API_VALIDATION_SERVICE, "SendGrid")
        api_key = params.get(KEY_API_KEY)
        auth_dict = self.get_auth_dict(api_key, validation_service)

        email_column = params.get(KEY_EMAIL_COLUMN_NAME)
        source_column = params.get(KEY_EMAIL_SOURCE_COLUMN_NAME)
        input_table = self.get_input_table()
        input_columns = self.get_input_columns(input_table.full_path)

        email_validator = EmailValidatorFactory.create_email_validator(validation_service)

        logging.info("Authorizing client")
        email_validator.authorize(auth_dict)
        email_validator.set_api_parameters(email_column=email_column, source_column=source_column)

        loading_options = params.get(KEY_LOADING_OPTIONS)
        incremental = loading_options.get(KEY_LOADING_OPTIONS_INCREMENTAL)
        pkey = loading_options.get(KEY_LOADING_OPTIONS_PKEY)

        results_output_table = self.create_out_table_definition("validated_emails.csv",
                                                                incremental=incremental,
                                                                primary_key=pkey,
                                                                columns=email_validator.output_columns)

        not_fetched_output_table = self.create_out_table_definition("not_validated_emails.csv",
                                                                    incremental=False,
                                                                    columns=input_columns)

        logging.info("Fetching and writing results")
        num_emails = self.get_number_of_emails_in_input(input_table.full_path)
        logging.info(f"Validating a total of {num_emails} emails")

        input_generator = self.input_file_reader(input_table)
        validated, not_validated = self.fetch_validator_results(email_validator, input_generator)

        self.write_results(validated, results_output_table.full_path, email_validator.output_columns)
        self.write_results(not_validated, not_fetched_output_table.full_path, input_columns)

        self.write_tabledef_manifest(results_output_table)
        self.write_tabledef_manifest(not_fetched_output_table)

    def fetch_validator_results(self, email_validator, input_generator):
        num_emails_validated = 0
        start_time, end_time = self.get_run_time(MAX_RUNTIME_SECONDS)
        input_row = True
        validated = []
        not_validated = []
        while True and input_row:
            current_time = self.get_current_time()
            if current_time > end_time:
                logging.warning("Time limit of component reached. Writing fetched results to storage")
                break
            input_row = next(input_generator, None)
            if input_row:
                validation_result = email_validator.validate_email(input_row)
                if validation_result:
                    validated.append(validation_result)
                else:
                    not_validated.append(input_row)
                num_emails_validated = num_emails_validated + 1
            else:
                break
            if num_emails_validated % LOGGING_MAILS_FREQUENCY == 0:
                logging.info(f"{num_emails_validated} have been validated so far, continuing")
        while True and input_row:
            input_row = next(input_generator, None)
            if input_row:
                not_validated.append(input_row)
        logging.info(
            f"Email fetching has been completed, a total of {num_emails_validated} have been validated, "
            f"{len(not_validated)} of those could not be validated")
        return validated, not_validated

    def get_input_table(self):
        input_tables = self.get_input_tables_definitions()
        if len(input_tables) == 0:
            raise UserException("No input table added. Please add an input table")
        elif len(input_tables) > 1:
            raise UserException("Too many input tables added. Please add only one input table")
        return input_tables[0]

    @staticmethod
    def input_file_reader(input_table):
        with open(input_table.full_path, mode='r') as in_file:
            reader = csv.DictReader(in_file)
            for input_row in reader:
                yield input_row

    @staticmethod
    def write_results(results, filepath, columns):
        with open(filepath, mode='wt', encoding='utf-8', newline='') as out_file:
            writer = csv.DictWriter(out_file, fieldnames=columns)
            for row in results:
                writer.writerow(row)

    @staticmethod
    def get_number_of_emails_in_input(full_path):
        with open(full_path, mode='r') as in_file:
            reader = csv.reader(in_file)
            return len(list(reader)) - 1

    @staticmethod
    def get_auth_dict(api_key, validation_service):
        if api_key:
            auth_dict = {"api_key": api_key}
        else:
            raise UserException(f"Authorization of email validator API not set, please fill in the required"
                                f" authorization method for the {validation_service} API")
        return auth_dict

    @staticmethod
    def get_current_time():
        return time.time()

    def get_run_time(self, max_runtime_seconds):
        start_time = self.get_current_time()
        end_time = start_time + max_runtime_seconds
        return start_time, end_time

    @staticmethod
    def get_input_columns(input_table):
        with open(input_table, "r") as f:
            d_reader = csv.DictReader(f)
            return d_reader.fieldnames


class EmailValidatorFactory:
    class ValidationService(Enum):
        SENDGRID = "SendGrid"

    @staticmethod
    def create_email_validator(validation_service: ValidationService):
        if validation_service == EmailValidatorFactory.ValidationService.SENDGRID.value:
            return validators.sendgrid_validator.SendgridEmailValidator()


if __name__ == "__main__":
    try:
        comp = Component()
        comp.run()
    except UserException as exc:
        logging.exception(exc)
        exit(1)
    except Exception as exc:
        logging.exception(exc)
        exit(2)
