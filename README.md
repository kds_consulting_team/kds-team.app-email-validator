# Email validator

This component allows you to validated emails using various API validation services.

*NOTE: This component currently only supports the SendGrid email validation service, for another service to 
be supported please contact support*

**Table of contents:**  
  
[TOC]

## Configuration

Add a table input containing emails you wish to validate, along with any further data the API supports

### Authentication

- API key (api_key) - [REQ] 
  - For SendGrid go to Settings > API Keys > Create API Key selecting Email Address Validation Access 
    permission and selecting full access in the validation section
    
### SendGrid configuration

- Email column (email_column) - [REQ] name of the column in the input table that contains the emails to be validated
- Source column (source_column) - [REQ] name of the column in the input table that contains the source/origin of emails to be validated eg. Newsletter signup
- Loading options (loading_options) - [REQ] What type of loading; Full overwrites the existing table in storage and incremental appends new and updates existing passes in the table using a primary key.
  - Primary Key - [OPT] Necessary for incremental load type