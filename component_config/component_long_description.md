This component enables users to validate emails from tables in Keboola for their uses in email marketing or other
usecases. The validator returns estimates on validity and performed checks. This component supports the following APIs : Sendgrid Email Address validation.
For another API to be added to this application, contact support. 